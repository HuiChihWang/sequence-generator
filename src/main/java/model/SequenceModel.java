package model;

public interface SequenceModel {
    boolean applyModel(int[] partialSequence);
    int[] generateFullSequence(int[] partialSequence, int generatedLength);
}
