package model;

import utility.LinearSolver;

public class LinearModel implements SequenceModel {
    // assume a(k) = b * a(k-1) + r;
    private int b = 1;
    private int r = 0;

    private final LinearSolver solver;

    public LinearModel() {
        this.solver = new LinearSolver();
    }

    @Override
    public boolean applyModel(int[] partialSequence) {
        if (partialSequence.length <= 2) {
            if (partialSequence.length == 2) {
                r = partialSequence[1] - partialSequence[0];
            }
            return true;
        }

        int[] params = solver.solveEquations(
                new int[]{partialSequence[0], 1, partialSequence[1]},
                new int[]{partialSequence[1], 1, partialSequence[2]}
        );

        if (params == null) {
            return false;
        }

        b = params[0];
        r = params[1];

        for (int i = 3; i < partialSequence.length; ++i) {
            int expect = partialSequence[i - 1] * b + r;
            if (expect != partialSequence[i]) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int[] generateFullSequence(int[] partialSequence, int generatedLength) {
        int totalLength = partialSequence.length + generatedLength;
        int[] generatedSequence = new int[totalLength];
        System.arraycopy(partialSequence, 0, generatedSequence, 0, partialSequence.length);

        for (int i = partialSequence.length; i < generatedSequence.length; ++i) {
            generatedSequence[i] = b * generatedSequence[i - 1] + r;
        }

        return generatedSequence;
    }
}
