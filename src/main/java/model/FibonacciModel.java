package model;

import utility.LinearSolver;

public class FibonacciModel implements SequenceModel {
    // a[k] = p1 * a[k-1] + p2 * a[k-2]
    int p1 = 1;
    int p2 = 1;

    private final LinearSolver solver;

    public FibonacciModel() {
        this.solver = new LinearSolver();
    }

    @Override
    public boolean applyModel(int[] partialSequence) {
        if (partialSequence.length <= 3) {
            return false;
        }

        int[] params = solver.solveEquations(
                new int[]{partialSequence[0], partialSequence[1], partialSequence[2]},
                new int[]{partialSequence[1], partialSequence[2], partialSequence[3]}
        );

        if (params == null) {
            return false;
        }

        p2 = params[1];
        p1 = params[0];

        for (int i = 4; i < partialSequence.length; ++i) {
            int expect = partialSequence[i - 1] * p1 + partialSequence[i-2] * p2;
            if (expect != partialSequence[i]) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int[] generateFullSequence(int[] partialSequence, int generatedLength) {
        int totalLength = partialSequence.length + generatedLength;
        int[] generatedSequence = new int[totalLength];
        System.arraycopy(partialSequence, 0, generatedSequence, 0, partialSequence.length);

        for (int i = partialSequence.length; i < generatedSequence.length; ++i) {
            generatedSequence[i] = p1 * generatedSequence[i - 1] + p2 * generatedSequence[i-2];
        }

        return generatedSequence;
    }
}
