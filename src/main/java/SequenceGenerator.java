import model.FibonacciModel;
import model.LinearModel;
import model.SequenceModel;

import java.util.Arrays;
import java.util.List;

public class SequenceGenerator {
    private final int lengthToGenerate;

    private int[] fullSequence;

    private final List<SequenceModel> models = List.of(
            new LinearModel(),
            new FibonacciModel()
    );

    public SequenceGenerator(int lengthToGenerate) {
        this.lengthToGenerate = lengthToGenerate;
    }

    public boolean generateFullSequence(int[] partialSequence) {
        for (SequenceModel model : models) {
            if (model.applyModel(partialSequence)) {
                System.out.println(model.getClass());
                fullSequence = model.generateFullSequence(partialSequence, lengthToGenerate);
                return true;
            }
        }

        return false;
    }

    public int[] getFullSequence() {
        return fullSequence;
    }

    public static void main(String[] args) {
        int[] partialSequence = parseInputsToArray(args);

        SequenceGenerator generator = new SequenceGenerator(10);
        boolean isSequenceGenerated = generator.generateFullSequence(partialSequence);
        if (isSequenceGenerated) {
            System.out.println(Arrays.toString(generator.getFullSequence()));
        } else {
            System.out.println("Cannot Guess Sequence Pattern.");
        }
    }

    private static int[] parseInputsToArray(String[] strInputs) {
        int[] inputs = new int[strInputs.length];

        for (int i = 0; i < strInputs.length; ++i) {
            inputs[i] = Integer.parseInt(strInputs[i]);
        }
        return inputs;
    }
}
