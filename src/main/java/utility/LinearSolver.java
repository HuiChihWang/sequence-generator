package utility;

// solver for following equation
// a1 x + b1 y = c1
// a2 x + b2 y = c2
public class LinearSolver {
    public int[] solveEquations(int[] firstParams, int[] secondParams) {
        int delta = firstParams[0] * secondParams[1] - secondParams[0] * firstParams[1];
        int deltaX = firstParams[2] * secondParams[1] - secondParams[2] * firstParams[1];
        int deltaY = firstParams[0] * secondParams[2] - secondParams[0] * firstParams[2];

        boolean solvable = deltaX % delta == 0 && deltaY % delta == 0;

        if (!solvable) {
            return null;
        }

        return new int[]{deltaX / delta, deltaY / delta};
    }
}
